<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<link rel="icon" href="img/ico.png">
<link type="text/css" rel="stylesheet" href="css/style-reg.css">
<script language="javascript" src="js/1.js"></script>
<title>Create a Koodle Account</title>
</head>
<!-- Coded by Wello6143. 2018. Aerons Tech Team. -->
<body>
<strong><div class="account-page">
  <div class="form">
    <form class="login-form" method="post">
      <h3 class="message">Create a <img src="img/koodle.png"> Account</h3>
      <p></p>
      <p></p>
      <p></p>
      <p class="message"><font size="-3">Please fill in all information below. All the boxes are required.</font></p>
      <p></p>
      <p></p>
      <p></p>
      <input type="text" placeholder="Name" name="name" title="Enter your name, we are not recommend you to use your nickname" required/>
      <input type="text" placeholder="Username" name="@username" title="Enter your username, this username will be using for everyone finding your account, log-in to your account and recover your password" required/>	  
      <input type="password" placeholder="Password" name="psw" minlength="6" maxlength="32" title="Enter your password, you'll use this for sign-in your account" required/>
      <input type="date" name="birth" min="1910-12-31" max="2016-12-31"/>
      <input type="text" placeholder="Email Address" name="email" title="Enter your email address, please use real Email address when recovery your password" required/>
      <p class="message">By clicking CREATE AN ACCOUNT, you've read and accepted to <a href="#">our terms and privacy policy</a> of Koodle Development Team.</p>
      <p></p>
      <p></p>
      <p></p>
      <a href="thankyou"><button>create an account</button></a>
      <p class="message">Already registered? <a href="sign_in">Click here to sign in</a>.</p>
      <p class="message">&copy; Koodle Development Team. 2018</p>
    </form>
  </div>
</div></strong>
</body>
</html>
