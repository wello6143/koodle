<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<link rel="icon" href="img/ico.png">
<link type="text/css" rel="stylesheet" href="css/style-reg.css">
<script language="javascript" src="js/1.js"></script>
<title>Sign in Koodle Account</title>
</head>
<!-- Coded by Wello6143. 2018. Aerons Tech Team. -->
<body>
<strong><div class="account-page">
  <div class="form">
    <form class="login-form" method="post">
      <h3 class="message">Sign in to your <img src="img/koodle.png"> Account</h3>
      <p></p>
      <p></p>
      <p></p>
      <input type="text" placeholder="Username" name="username"/>
      <input type="password" placeholder="Password" name="psw"/>
      <a href="thankyou"><button>sign in</button></a>
      <p class="message"><a href="#">Forgot password?</a></p>
      <p class="message">Not registered? <a href="register">Click here to create one!</a>.</p>
      <p class="message">&copy; Koodle Development Team. 2018</p>
    </form>
  </div>
</div></strong>
</body>
</html>
