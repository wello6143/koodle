<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="css/style.css">
<link rel="icon" href="img/ico.png">
<script language="javascript" src="js/detectmobilebrowser.js"></script>
<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<head>
	<title>Welcome to Koodle - A place for creators and everyones!</title>
</head>
<body>
	<div class="bg-pur container-fix twothird fix">
    	<h4 class="message">Koodle is a social network where you can get the best things from brilliant creators, where you can join and enjoy too!</h4>
		<ul id="icons">
			<li class="ion-ios-locked-outline" data-pack="ios" data-tags="security, padlock, safe"> No spy, no leaking informations</li>
			<li class="ion-ios-people-outline"> Join awesomeness communities</li>
			<li class="ion-ios-world-outline"> Get lastest news, updates from creators</li>
			<li class="ion-ios-game-controller-b-outline"> Connect, upload and stream with YOLO Entertainment Network (not working yet)</li>
			<li class="ion-happy-outline"> At Koodle, fun, happy and suprises are unlimited</li>
			<li class="ion-ios-more-outline"> and more...</li>
			<li>What are you waiting for? Join now!</li>
		</ul>
    </div>
	<div class="third container-fix fix">
		<h4 class="message-bl message">Sign in</h4>
		<div>
			<form class="form">
				<input type="text" placeholder="Username or Email" required>
				<input type="password" placeholder="Password" required>
				<button>sign in</button>
				<a href="register.php"><p class="forgot-reg"><font class="reg">Register an account</font><font class="forgot">Forgot Password?</font></p></a>
			</form>
		</div>
	</div>
	<div class="footer">
		<p class="footer-a"><a  href="about.php">About us</a> <a  href="donate.php">Donate</a> <a  href="#">Feedback</a> <a  href="#">Server status</a> <a href="#">Languages</a> <a href="#">Privacy Policy</a> <a class="disabled">&copy;Koodle Development Team. 2018</a></p>
	</div>
</body>
</html>