<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link href="img/ico.png" rel="icon">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
<link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
<title>Thank you for visited our demo</title>
<style>
body{
	font-family: 'Cabin', sans-serif;
}
p{
	color: #FFF;
	font-size: 21px;
}
a{
	text-decoration: none;
	color: #FFF;
}
</style>
</head>
<body bgcolor="#8200fa">
<h1>Thank you for visited out demo page!</h1>
<p>You came to this place from our Twitter page, right? We have just coded the front-end for this demo, if you can code the back-end (PHP, Node.js or whatever to code the back-end for the website) please contact us on Twitter: <a href="https://twitter.com/wello6143">@wello6143</a> or <a href="https://twitter.com/real_cheete">@real_cheete</a>. We are highly appreciate your help.</p>
<p>P.s. My English is quite bad :v (Wello6143).</p>
</body>
</html>
